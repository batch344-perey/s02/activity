import java.util.Arrays;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
            int[] primeNumberArray = {2, 3, 5, 7, 11};

        System.out.println("The first prime number is: " + primeNumberArray[0]);
        System.out.println("The second prime number is: " + primeNumberArray[1]);
        System.out.println("The third prime number is: " + primeNumberArray[2]);
        System.out.println("The fourth prime number is: " + primeNumberArray[3]);
        System.out.println("The fifth prime number is: " + primeNumberArray[4]);

    }
}