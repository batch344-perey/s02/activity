import java.util.ArrayList;

public class forArrayList {
    public static void main(String[] args) {
        ArrayList<String> friends = new ArrayList<String>();

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);
    }
}
