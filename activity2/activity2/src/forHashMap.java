import java.util.HashMap;

public class forHashMap {
    public static void main(String[] args) {
        HashMap<String, Integer> inventory = new HashMap<String, Integer>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);
    }
}
