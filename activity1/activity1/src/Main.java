import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        Scanner yearScanner = new Scanner(System.in);
        System.out.println("Enter a year:");

        int yearValue = yearScanner.nextInt();

        if (((yearValue % 4 == 0) && (yearValue % 100!= 0)) || (yearValue%400 == 0)) {
            System.out.println(yearValue + " is a leap year");
        } else {
            System.out.println(yearValue + " is not a leap year");
        }
    }
}